import { Component, OnInit } from '@angular/core';

/**
 * Component displaying an animated loader giving the effect of a ripple
 */
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
