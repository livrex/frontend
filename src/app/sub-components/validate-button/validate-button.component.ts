import { Component, Input, OnInit } from '@angular/core';

/**
 * Green validate button
 */
@Component({
  selector: 'app-validate-button',
  templateUrl: './validate-button.component.html',
  styleUrls: ['./validate-button.component.css']
})
export class ValidateButtonComponent implements OnInit {

  @Input() body: string = ' ';
  @Input() disabled: boolean = false;
  
  constructor() { }

  ngOnInit(): void {
  }

}
