import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Colors } from 'src/app/enums/colors.enum';

/**
 * Component replacing the default checkbox style
 */
@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() colorType: Colors = Colors.PRIMARY;
  @Input() checked = false;
  @Output() change = new EventEmitter<Event>();
  @Output() onClick = new EventEmitter<Event>();

  constructor() { }

  clickEvent(event: Event){
    this.checked = !this.checked;
    this.onClick.emit(event);
  }

  ngOnInit(): void {
  }

}
