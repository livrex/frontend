import { Component, Input, OnInit } from '@angular/core';
import { Path } from 'src/app/interfaces/path';

/**
 * (Unused for now) Component displaying the graph of a path
 */
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  @Input() path: Path;
  xMax: number;
  yMax: number;
  @Input() checkpoints: number[];

  constructor() { }

  ngOnInit(): void {
    this.xMax = Math.max(...this.path.path.map(point => point['x']));
    this.yMax = Math.max(...this.path.path.map(point => point['y']));
    if(this.checkpoints == undefined){
      this.checkpoints = new Array(this.path.path.length).fill(0);
      this.checkpoints[0] = 1;
    }
  }

}
