import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * Replaces the default slider with a gray slider with a blue handle
 */
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  @Input() body: string = '';
  @Output() input = new EventEmitter<Event>();

  constructor() { }

  ngOnInit(): void {
  }

}
