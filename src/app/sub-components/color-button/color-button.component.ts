import { Component, Input, OnInit } from '@angular/core';
import { ColorComputationService } from 'src/app/services/color-computation.service';

/**
 * Button component gradually changing color
 */
@Component({
  selector: 'app-color-button',
  templateUrl: './color-button.component.html',
  styleUrls: ['./color-button.component.css']
})
export class ColorButtonComponent implements OnInit {

  /**
   * A collection of colors in strings and in a CSS valid syntax
   */
  @Input() colorArray: string[];

  constructor(private colorComputation: ColorComputationService) { }

  /**
   * Formats the colors for the CSS
   */
  getColorToStyle(){
    let res = 'linear-gradient(';
    this.colorArray.forEach(color => res += color + ',');
    if(this.colorArray.length === 1) res += this.colorComputation.getShadeOfColor(this.colorArray[0], -40) + ',';
    res = res.slice(0, -1) + ')';
    return res;
  }

  ngOnInit(): void {
  }

}
