import { Component, Input, OnInit } from '@angular/core';

/**
 * Simple button with white background and black text
 */
@Component({
  selector: 'app-simple-button',
  templateUrl: './simple-button.component.html',
  styleUrls: ['./simple-button.component.css']
})
export class SimpleButtonComponent implements OnInit {

  @Input() body: string = ' '; 

  constructor() { }

  ngOnInit(): void {
  }

}
