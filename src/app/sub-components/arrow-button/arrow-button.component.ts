import { Component, Input, OnInit } from '@angular/core';

/**
 * Button component displaying an arrow on hover
 */
@Component({
  selector: 'app-arrow-button',
  templateUrl: './arrow-button.component.html',
  styleUrls: ['./arrow-button.component.css']
})
export class ArrowButtonComponent implements OnInit {

  @Input() body: string = ' ';
  @Input() backwards: boolean = true; // the direction of the arrow
  @Input() disabled: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
