import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplexGraphComponent } from './complex-graph.component';

describe('ComplexGraphComponent', () => {
  let component: ComplexGraphComponent;
  let fixture: ComponentFixture<ComplexGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplexGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
