import { Component, Input, OnInit } from '@angular/core';
import { Coordinates2D } from 'src/app/interfaces/coordinates';
import { Path } from 'src/app/interfaces/path';
import { TravelPoint } from 'src/app/interfaces/travel-point';
import { ColorComputationService } from 'src/app/services/color-computation.service';
import { GraphComputationService } from 'src/app/services/graph-computation.service';

import { Colors } from "../../enums/colors.enum";

/**
 * Component displaying a graph with all the paths and points provided and options to manipulate it
 */
@Component({
  selector: 'app-complex-graph',
  templateUrl: './complex-graph.component.html',
  styleUrls: ['./complex-graph.component.css']
})
export class ComplexGraphComponent implements OnInit {

  @Input() paths: Path[];
  @Input() forTomorrow: TravelPoint[]; // items that could not be processed due to the 240 km limit per driver
  
  @Input() checkpointsArray: number[][]; // unused for now

  colors: typeof Colors = Colors;
  maxCoordinates: Coordinates2D;
  showDriver: boolean[];
  tempShowDriver: boolean[]; // useful to save the previous state of showDriver
  circleRadius: number = 70;
  strokeWidth: number = 1;
  graphSize: number = 50;
  graphPosition: Coordinates2D = {x: 0, y: 0}; // used to recenter the graph
  optionsHeight: OptionsHeight = OptionsHeight.DEFAULT;
  colorArray: string[] = [];
  showPathLength: boolean[] = [];
  showItemsID: boolean[] = [];
  showItemsCoordinates: boolean[] = [];

  constructor(private graphComputation: GraphComputationService, private colorComputation: ColorComputationService) { }

  ngOnInit(): void {
    // initializing all the arrays and getting the necessary values for the graph
    this.maxCoordinates = this.graphComputation.pathArrayMax(this.paths);
    if(this.checkpointsArray == undefined){
      this.checkpointsArray = this.graphComputation.generateCheckpointsArray(this.paths);
    }
    this.showDriver = new Array(this.paths.length).fill(true);
    this.paths.forEach(path => {
        this.colorArray.push(this.colorComputation.getRandomColor());
        this.showPathLength.push(false);
        this.showItemsID.push(false);
        this.showItemsCoordinates.push(false);
      }
    );
  }

  //Below are all the methods used to convert the convert the coordinates in percentages relative to 'top' or 'left' in css
  
  coordinateConversion(coordinate: number, maximum: number){
    return (coordinate*(90/2)/(maximum)) + 45;
  }

  convertXCoordinateLine(initialCoordinate: number){
    return this.coordinateConversion(initialCoordinate, this.maxCoordinates.x) + 5; // 5 is the radius of the bubble in percent
  }

  convertYCoordinateLine(initialCoordinate: number){
    return this.coordinateConversion(-initialCoordinate, this.maxCoordinates.y) + 5; // 5 is the radius of the bubble in percent
  }

  convertXCoordinatePoint(initialCoordinate: number){
    return this.coordinateConversion(initialCoordinate, this.maxCoordinates.x);
  }

  convertYCoordinatePoint(initialCoordinate: number){
    return this.coordinateConversion(-initialCoordinate, this.maxCoordinates.y);
  }

  calculateXMiddleCoordinate(firstPoint: number, secondPoint: number){
    return (this.convertXCoordinateLine(firstPoint) + this.convertXCoordinateLine(secondPoint))/2;
  }

  calculateYMiddleCoordinate(firstPoint: number, secondPoint: number){
    return (this.convertYCoordinateLine(firstPoint) + this.convertYCoordinateLine(secondPoint))/2;
  }

  //Driver selection methods

  highlightDriver(index){
    this.tempShowDriver = [...this.showDriver];
    this.showDriver = new Array(this.tempShowDriver.length).fill(false);
    this.showDriver[index] = true;
  }

  removeHighlightDriver(){
    this.showDriver = [...this.tempShowDriver];
  }

  selectDriver(index){
    this.tempShowDriver[index] = !this.tempShowDriver[index];
  }

  //Graph options methods

  adjustCircleRadius(event){
    this.circleRadius = event.target.value;
  }

  adjustStrokeWidth(event){
    this.strokeWidth = event.target.value/50;
  }

  increaseZoom(){
    this.graphSize += 10;
  }

  decreaseZoom(){
    this.graphSize -= 10;
  }

  resetGraph(){
    this.graphSize = 50;
    this.graphPosition = {x: 0, y: 0}
  }

  // simple round method for the html template
  roundNumber(n: number){
    return Math.round(n);
  }

  //getter to use the service
  get getShadeOfColor(){
    return this.colorComputation.getShadeOfColor;
  }

  //generates color for all the paths
  generateColors(){
    this.colorArray = [];
    this.paths.forEach(path => this.colorArray.push(this.colorComputation.getRandomColor()));
  }

  //methods to display more info on the graph

  showDetails(index: number){
    this.showPathLength[index] = !this.showPathLength[index];
  }

  showID(index: number){
    this.showItemsID[index] = !this.showItemsID[index];
  }

  showCoordinates(index: number){
    this.showItemsCoordinates[index] = !this.showItemsCoordinates[index];
  }

  //methods to adjust the size of the infoBox with the info on the paths

  increaseOptions(){
    if(this.optionsHeight === OptionsHeight.DEFAULT)
      this.optionsHeight = OptionsHeight.EXPANDED;
    else if(this.optionsHeight === OptionsHeight.HIDDEN)
      this.optionsHeight = OptionsHeight.DEFAULT;      
  }

  decreaseOptions(){
    if(this.optionsHeight === OptionsHeight.DEFAULT)
      this.optionsHeight = OptionsHeight.HIDDEN;
    else if(this.optionsHeight === OptionsHeight.EXPANDED)
      this.optionsHeight = OptionsHeight.DEFAULT;   
  }

  checkOptionsHeightExpanded(){
    return this.optionsHeight === OptionsHeight.EXPANDED;
  }

  checkOptionsHeightHidden(){
    return this.optionsHeight === OptionsHeight.HIDDEN;
  }
}


enum OptionsHeight {
  HIDDEN = 0,
  DEFAULT = 30,
  EXPANDED = 90,
}