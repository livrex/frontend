import { useAnimation, trigger, transition, style, query, animateChild, animate, group } from '@angular/animations';
import { leftRight, rightLeft, topBottom } from "./animations";
/**
 * Animations used for the routes of the app
 */
export const slideInAnimation =
  trigger('routeAnimations', [
    
    transition('HomePage => FormPage', [
      useAnimation(topBottom)
    ]),
    
    transition('FormPage => LoadingPage', [
      useAnimation(rightLeft)
    ]),

    transition('LoadingPage => PathsPage', [
      useAnimation(rightLeft)
    ]),
    
    transition('PathsPage => FormPage', [
      useAnimation(leftRight)
    ])
  ]);