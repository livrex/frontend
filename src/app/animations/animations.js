import { animation, style, query, animateChild, animate, group } from '@angular/animations';

/**
 * Left to right transition
 */
export const leftRight = animation([
    style({ position: 'relative', overflow: 'hidden'}),
    query(':enter, :leave', [ // Style common for both pages at the start
        style({ // centre
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%'
        })
    ]),
    query(':enter', [
        style({ left: '-100%', opacity: 0 }) // placing the new page on the left outside of view
    ]),
    query(':leave', [
        style({opacity: 1 })
    ]),
    query(':leave', animateChild()),
    group([ // grouping simultaneous animations
        query(':leave', [
            animate('300ms ease-out', style({ left: '100%', opacity: 0 })) // removing old page
        ]),
        query(':enter', [
            animate('300ms ease-out', style({ left: '0%', opacity: 1 })) // placing new page
        ])
    ]),
    query(':enter', animateChild()),
]);

/**
 * Transition from right to left
 */
export const rightLeft = animation([
    style({ position: 'relative', overflow: 'hidden' }),
    query(':enter, :leave', [
        style({
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%'
        })
    ]),
    query(':enter', [
        style({ left: '100%', opacity: 0 })
    ]),
    query(':leave', animateChild()),
    group([
        query(':leave', [
            animate('300ms ease-out', style({ left: '-100%' }))
        ]),
        query(':enter', [
            animate('300ms ease-out', style({ left: '0%', opacity: 1 }))
        ])
    ]),
    query(':enter', animateChild()),
]);

/**
 * Transition from top to bottom
 */
export const topBottom = animation([
    style({ position: 'relative', overflow: 'hidden' }),
    query(':enter', [
        style({
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
        })
    ]),
    query(':enter', [
        style({ top: '100vh' })
    ]),
    query(':leave', animateChild()),
    group([
        query(':enter', [
            animate('500ms ease-out', style({ top: 0 }))
        ])
    ]),
    query(':enter', animateChild()),
]);