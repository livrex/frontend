import { Component, OnInit } from '@angular/core';
import { Path } from '../interfaces/path';
import { TravelPoint } from '../interfaces/travel-point';
import { PathDataService } from '../services/path-data.service';

/**
 * Page displaying the graph with the paths received from the backend
 */
@Component({
  selector: 'app-paths-overview',
  templateUrl: './paths-overview.component.html',
  styleUrls: ['./paths-overview.component.css']
})
export class PathsOverviewComponent implements OnInit {

  paths: Path[];
  forTomorrow: TravelPoint[];

  // Both variables unused for now
  checkpoints: number[];
  checkpointsDone: number;
  
  constructor(private pathData: PathDataService) { }
    
  ngOnInit(): void {
    // getting the information received from the backend
    this.pathData.currentPath.subscribe(
      data => {
        this.paths = data[0];
        this.forTomorrow = data[1];
      }
    )
  }

  /**
   * (Unused for now) Method to display the next travel point
   */
  validateCheckpoint(){
    this.checkpoints[this.checkpointsDone] = 1;
    this.checkpoints[this.checkpointsDone + 1] = 2;
    this.checkpointsDone++;
  }

}
