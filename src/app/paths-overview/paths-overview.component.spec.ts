import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PathsOverviewComponent } from './paths-overview.component';

describe('PathsOverviewComponent', () => {
  let component: PathsOverviewComponent;
  let fixture: ComponentFixture<PathsOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PathsOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PathsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
