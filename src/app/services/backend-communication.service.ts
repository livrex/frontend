import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Path } from '../interfaces/path';
import { TravelPoint } from '../interfaces/travel-point';
import { environment } from 'src/environments/environment';
import { BackendResponse } from '../interfaces/backend-response';

/**
 * Service containing the requests that can be made to the backend
 */
@Injectable({
  providedIn: 'root'
})
export class BackendCommunicationService {

  private defaultHeaders = new HttpHeaders()
  .set('Content-type', 'application/json');

  constructor(private http: HttpClient) { }

  /**
   * Sends the a list of items and drivers, receives a distribution in paths 
   * @param drivers a list of drivers with their info
   * @param items a list of items with their info
   * @returns the different paths computed by the backend, or an error message
   */
  async sendItems(drivers: TravelPoint[], items: TravelPoint[]): Promise<BackendResponse|HttpErrorResponse> {
    
    let response: BackendResponse|HttpErrorResponse;
    await this.http.post<any>(
      environment.baseUrl + '/spt',
        {
          drivers,
          items
        }
      ,
      {
        headers: this.defaultHeaders
      }
    )
      .toPromise()
      .then(
        (data: BackendResponse) => { // Success
          response = data;
        },
        (err: HttpErrorResponse) => { // Error
          response = err.error;
        }
      );

    return response;
  }
}
