import { TestBed } from '@angular/core/testing';

import { ColorComputationService } from './color-computation.service';

describe('ColorComputationService', () => {
  let service: ColorComputationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColorComputationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
