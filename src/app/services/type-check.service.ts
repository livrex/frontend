import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BackendResponse } from '../interfaces/backend-response';
import { Path } from '../interfaces/path';

/**
 * Service containing methods to check certain types
 */
@Injectable({
  providedIn: 'root'
})
export class TypeCheckService {

  constructor() { }

  /**
   * Method to verify the nature of the backend's response
   * @param res 
   * @returns if res is of BackendResponse type
   */
  isBackendResponse(res: BackendResponse | HttpErrorResponse): res is BackendResponse {
    return (res as BackendResponse).tomorrow !== undefined;
  }

  //Both methods below are unused for now
  isPath(res: Path | HttpErrorResponse): res is Path {
    return (res as Path).distance !== undefined;
  }

  isPathArray(res: Path[] | HttpErrorResponse): res is Path[] {
    return (res as Path[])[0].distance !== undefined;
  }
  
}
