import { TestBed } from '@angular/core/testing';

import { GraphComputationService } from './graph-computation.service';

describe('GraphComputationService', () => {
  let service: GraphComputationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GraphComputationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
