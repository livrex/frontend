import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Path } from '../interfaces/path';
import { TravelPoint } from '../interfaces/travel-point';

/**
 * Service storing the info entered by the user (drivers, items), the info to send to the backend and received
 * from it (paths generated)
 */
@Injectable({
  providedIn: 'root'
})
export class PathDataService {

  private toSend = new BehaviorSubject<[any[], any[]]>(null);
  private paths = new BehaviorSubject<[Path[], TravelPoint[]]>(null);
  private formInfo = new BehaviorSubject<[FormGroup, boolean[], boolean[]]>(null);

  /**
   * creating observables to get the values
   */
  currentToSend = this.toSend.asObservable();
  currentPath = this.paths.asObservable();
  currentForm = this.formInfo.asObservable();

  constructor() { }

  /**
   * Updatesthe values to send to the backend
   * @param drivers
   * @param items
   */
  updateToSend(drivers, items): void {
    this.toSend.next([drivers, items]);
  }

  /**
   * Updates info received from the backend
   * @param paths 
   * @param forTomorrow 
   */
  updatePath(paths: Path[], forTomorrow): void {
    this.paths.next([paths, forTomorrow]);
  }

  /**
   * Updates the form's value
   * @param formInfo 
   */
  updateForm(formInfo: [FormGroup, boolean[], boolean[]]): void {
    this.formInfo.next(formInfo);
  }
}
