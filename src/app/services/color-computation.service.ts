import { Injectable } from '@angular/core';

/**
 * Service containing color related computations in hexadecimal (with #)
 */
@Injectable({
  providedIn: 'root'
})
export class ColorComputationService {

  constructor() { }

  /**
   * Generates a random color
   * @returns the color in string form
   */
  getRandomColor(){
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  /**
   * Method to get a lighter or darker shade of a color
   * @param col the base color
   * @param amt the amount to scale to 
   * (multiples of 40 work well, positive for lighter, negative for darker)
   * @returns the color in string form
   */
  getShadeOfColor(col, amt){
    col = col.replace(/^#/, '');
    if (col.length === 3) col = col[0] + col[0] + col[1] + col[1] + col[2] + col[2];

    let [r, g, b] = col.match(/.{2}/g);
    ([r, g, b] = [parseInt(r, 16) + amt, parseInt(g, 16) + amt, parseInt(b, 16) + amt]);

    r = Math.max(Math.min(255, r), 0).toString(16);
    g = Math.max(Math.min(255, g), 0).toString(16);
    b = Math.max(Math.min(255, b), 0).toString(16);

    const rr = (r.length < 2 ? '0' : '') + r;
    const gg = (g.length < 2 ? '0' : '') + g;
    const bb = (b.length < 2 ? '0' : '') + b;

    return `#${rr}${gg}${bb}`;
  }

}
