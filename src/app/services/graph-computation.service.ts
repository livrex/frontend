import { Injectable } from '@angular/core';
import { Coordinates2D } from '../interfaces/coordinates';
import { Path } from '../interfaces/path';

/**
 * Service containing graph computations methods
 */
@Injectable({
  providedIn: 'root'
})
export class GraphComputationService {

  constructor() { }

  /**
   * Method to return x and y absolute max of a path
   * @param pathInfo 
   * @returns x max and y max
   */
  pathMax(pathInfo: Path): Coordinates2D{
    let x = Math.max(...pathInfo.path.map(point => Math.abs(point['x']) ));
    let y = Math.max(...pathInfo.path.map(point => Math.abs(point['y']) ));
    return {x, y};
  }

  /**
   * Method to return x and y absolute max of a collection of paths
   * @param paths
   * @returns x max and y max
   */
  pathArrayMax(paths: Path[]): Coordinates2D{
    let x = -Infinity;
    let y = -Infinity;
    let temp: Coordinates2D;
    for (const path of paths) {
      temp = this.pathMax(path);
      if(temp.x > x) x = temp.x;
      if(temp.y > y) y = temp.y;
    }
    return {x, y}
  }

  /**
   * (Unused for now) Method to initialize the checkpoint array of a path
   * @param pathInfo the path concerned
   * @returns an array with the checkpoint value for each travel point
   */
  generateCheckpoints(pathInfo: Path): number[]{
    let checkpoints = new Array(pathInfo.path.length).fill(0);
    checkpoints[0] = 1;
    return checkpoints;
  }

  /**
   * (Unused for now) Method to initialize the checkpoint arrays for multiple paths
   * @param paths the paths concerned
   * @returns a 2 dimensional array with the checkpoint value for each travel point, for each path
   */
  generateCheckpointsArray(paths: Path[]){
    let checkpointsArray: number[][] = [];
    for (const path of paths) {
      checkpointsArray.push(this.generateCheckpoints(path));
    }
    return checkpointsArray;
  }
}
