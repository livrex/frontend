import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendCommunicationService } from '../services/backend-communication.service';
import { PathDataService } from '../services/path-data.service';
import { TypeCheckService } from '../services/type-check.service';
import { first } from 'rxjs/operators';

/**
 * Page displaying a loader and making a request to the backend using the info entered by the user
 */
@Component({
  selector: 'app-loading-page',
  templateUrl: './loading-page.component.html',
  styleUrls: ['./loading-page.component.css']
})
export class LoadingPageComponent implements OnInit {

  constructor(
    private _backendService: BackendCommunicationService,
    private pathData: PathDataService,
    private typeCheck: TypeCheckService,
    private router: Router
    ) { }

  ngOnInit(): void {
      this.pathData.currentToSend
      .pipe(first(val => val != null)) // taking only the first value
      .subscribe(toSend =>{
        let drivers = toSend[0];
        let items = toSend[1];
        this._backendService.sendItems(drivers, items).then(
          res => {
            if(this.typeCheck.isBackendResponse(res)) {
              this.pathData.updatePath(res.today, res.tomorrow);
              setTimeout(() => {
                this.router.navigate(['paths-overview']);
              }, 1000); // timeout to have a smooth transition if the response if immediate
            }
            else {
              alert('Something went wrong');
              this.router.navigate(['form']);
            }
          }
        );
      })
    
    
  }

}
