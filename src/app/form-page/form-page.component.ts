import { trigger, transition, style, animate } from '@angular/animations';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { PathDataService } from '../services/path-data.service';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

/**
 * Page to display a form to enter the information concerning the items to deliver
 * and the drivers
 */
@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.css'],
  animations: [
    //Simple animation for the rows of the form to enter the view smoothly
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ height: 0, opacity: 0 }),
            animate('500ms ease-out',
                    style({ height: '*', opacity: 1 })) // automatic calculation
          ]
        )
      ]
    )
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormPageComponent implements OnInit {
  
  orderForm: FormGroup; //the main form object
  items: FormArray;
  drivers: FormArray;
  show: boolean = false; //shows the next step of the form, to fill in the drivers' information

  //keeps track of what is checked, in order to send only the checked rows to the backend
  checkedRowsItems: boolean[] = [false];
  checkedRowsDrivers: boolean[] = [false];
  
  constructor(
    private formBuilder: FormBuilder, 
    private pathData: PathDataService,
    private router: Router
    ){
    this.orderForm = this.formBuilder.group({
      items: this.formBuilder.array([ this.createItem() ]),
      drivers: this.formBuilder.array([ this.createDriver() ]),
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      type: 'item',
      id: ['', RxwebValidators.unique()], //making sure the ids are unique
      x: '',
      y: ''
    });
  }

  get itemsArray(){
    return this.orderForm.get('items') as FormArray;
  }

  addItem(): void {
    this.items = this.itemsArray;
    this.items.push(this.createItem());
  }

  //unused for now
  removeItem() {
    this.items.removeAt(this.items.length - 1);
  }

  createDriver(): FormGroup {
    return this.formBuilder.group({
      type: 'driver',
      id: ['', RxwebValidators.unique()],
      x: '',
      y: ''
    });
  }

  get driversArray(){
    return this.orderForm.get('drivers') as FormArray;
  }

  addDriver(): void {
    this.drivers = this.driversArray;
    this.drivers.push(this.createDriver());
  }

  removeDriver() {
    this.drivers.removeAt(this.drivers.length - 1);
  }

  /**
   * Method that adds the type of row needed if conditions are met, that is when the user
   * types in the last row of the selection
   * @param index the index of the row
   * @param last boolean indicating if it is the last row
   * @param event event to get the value typed in by the user
   */
  addController(index: number, last: boolean, event){
    if(last && event.target.value.length > 0){
      if(this.show){
        this.addDriver();
        this.checkedRowsDrivers[index] = true;
        this.checkedRowsDrivers.push(false);
      } else {
        this.addItem();
        this.checkedRowsItems[index] = true;
        this.checkedRowsItems.push(false);
      }
    }
  }

  //unused for now
  removeController(){
    if(this.show) this.removeDriver();
    else this.removeItem();
    this.checkedRowsDrivers.pop();
  }

  /**
   * Method updating the values of the form and navigating to the loading page
   */
  onSubmit(){
    let drivers = this.orderForm.value.drivers.filter((el, index) => this.checkedRowsDrivers[index]);
    let items = this.orderForm.value.items.filter((el, index) => this.checkedRowsItems[index]);
    this.pathData.updateToSend(drivers, items);
    this.pathData.updateForm([this.orderForm, this.checkedRowsItems, this.checkedRowsDrivers]);
    this.router.navigate(['loading-page'], {skipLocationChange: true});
  }
  
  /**
   * Checks or unchecks an input of type check by updating the value of the corresponding array
   * @param event event to get the input value
   * @param index the index of the row
   */
  checkRow(event, index){
    if(this.show)
      this.checkedRowsDrivers[index] = event.target.checked;
    else
      this.checkedRowsItems[index] = event.target.checked;
  }

  /**
   * Method deciding if the input should be required for the form
   * @param array the formarray the input belongs to
   * @param first boolean indicating if it is the first row
   * @param last boolean indicating if it is the last row
   * @param index the index of the row
   * @returns true if valid or false otherwise
   */
  requiredConditions(array: FormArray, first: boolean, last: boolean, index: number){
    return (first && array.controls.length == 1) || (!last && this.checkedRowsItems[index]);
  }

  /**
   * Method deciding if the items form is invalid
   * @returns true if invalid or false otherwise
   */
  itemsButtonCondition(){
    return !this.orderForm.valid || (this.checkedRowsItems.filter(x => x).length == 0);
  }

  /**
   * Method deciding if the drivers form is invalid
   * @returns true if invalid or false otherwise
   */
  driversButtonCondition(){
    return !this.orderForm.valid || (this.checkedRowsDrivers.filter(x => x).length == 0);
  }

  ngOnInit() {
    //getting the stored state of the form if it is defined
    this.pathData.currentForm.subscribe(
      value => {
        if(value != null) {
          this.orderForm = value[0];
          this.checkedRowsItems = value[1];
          this.checkedRowsDrivers = value[2];
          this.show = true;
        }
      }
    )
  }
  
}