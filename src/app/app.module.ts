import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartButtonComponent } from './start-button/start-button.component';
import { FormPageComponent } from './form-page/form-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NavigationComponent } from './navigation/navigation.component';
import { GraphComponent } from './sub-components/graph/graph.component';
import { PathsOverviewComponent } from './paths-overview/paths-overview.component';
import { ComplexGraphComponent } from './sub-components/complex-graph/complex-graph.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { LoaderComponent } from './sub-components/loader/loader.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';

import {  RxReactiveFormsModule } from "@rxweb/reactive-form-validators";
import { CheckboxComponent } from './sub-components/checkbox/checkbox.component';
import { ArrowButtonComponent } from './sub-components/arrow-button/arrow-button.component';
import { ValidateButtonComponent } from './sub-components/validate-button/validate-button.component';
import { ColorButtonComponent } from './sub-components/color-button/color-button.component';
import { SliderComponent } from './sub-components/slider/slider.component';
import { SimpleButtonComponent } from './sub-components/simple-button/simple-button.component'


@NgModule({
  declarations: [
    AppComponent,
    StartButtonComponent,
    FormPageComponent,
    NavigationComponent,
    GraphComponent,
    PathsOverviewComponent,
    ComplexGraphComponent,
    LoaderComponent,
    LoadingPageComponent,
    CheckboxComponent,
    ArrowButtonComponent,
    ValidateButtonComponent,
    ColorButtonComponent,
    SliderComponent,
    SimpleButtonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    DragDropModule,
    RxReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
