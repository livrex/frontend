import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Homepage with a start button
 */
@Component({
  selector: 'app-start-button',
  templateUrl: './start-button.component.html',
  styleUrls: ['./start-button.component.css']
})
export class StartButtonComponent implements OnInit {

  isActive: boolean = false;

  constructor(private router: Router) { }

  onClick(){
    this.isActive = true;
    setTimeout(() => {
      this.router.navigate(['form']);
    }, 200); // allows the animation to finish before changing page
  }

  ngOnInit(): void {
  }

}
