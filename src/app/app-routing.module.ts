import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { FormPageComponent } from './form-page/form-page.component';
import { LoadingPageComponent } from './loading-page/loading-page.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PathsOverviewComponent } from './paths-overview/paths-overview.component';
import { StartButtonComponent } from './start-button/start-button.component';


const routes: Routes = [
  {
    path: '',
    component: StartButtonComponent,
    data: {animation: 'HomePage'}
  },
  {
    path: 'form',
    component: FormPageComponent,
    data: {animation: 'FormPage'}
  },
  {
    path: 'paths-overview',
    component: PathsOverviewComponent,
    data: {animation: 'PathsPage'}
  },
  {
    path: 'navigation',
    component: NavigationComponent,
    data: {animation: 'NavigationPage'}
  },
  {
    path: 'loading-page',
    component: LoadingPageComponent,
    data: {animation: 'LoadingPage'}
  }
];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
