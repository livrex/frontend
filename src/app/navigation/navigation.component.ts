import { Component, OnInit } from '@angular/core';
import { Path } from '../interfaces/path';
import { PathDataService } from '../services/path-data.service';

/**
 * Component unused for now, was supposed to display a single route 
 * and work like a GPS navigator for a delivery man
 * */ 
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  
  path: Path;
  checkpoints: number[];
  checkpointsDone: number;
  
  constructor(private pathData: PathDataService) { }
    
  ngOnInit(): void {
    this.pathData.currentPath.subscribe(
      data => {
        this.path = data[0][1];
      }
    )
    this.checkpoints = new Array(this.path.path.length).fill(0); // initializing checkpoints
    this.checkpoints[0] = 1; // by default starts at first travel point
    this.checkpoints[1] = 2;
    this.checkpointsDone = 1;
  }

  /**
   * Method to update the UI to display the next travel point
   */
  validateCheckpoint(){
    this.checkpoints[this.checkpointsDone] = 1;
    this.checkpoints[this.checkpointsDone + 1] = 2;
    this.checkpointsDone++;
  }
      
}
    