import { TravelPoint } from './travel-point';

export interface Path {
    distance: {
        edges: number[],
        sum: number
    },
    path: TravelPoint[]
}
