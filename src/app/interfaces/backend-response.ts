import { Path } from './path';
import { TravelPoint } from './travel-point';

export interface BackendResponse {
    today: Path[],
    tomorrow: TravelPoint[]
}
