export interface TravelPoint {
    type: string,
    id: string,
    x: number,
    y: number
}
