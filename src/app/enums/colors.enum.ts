/**
 * The main colors of the project in rgb for dynamic styling
 */
export enum Colors {
    PRIMARY = 'rgba(81, 203, 238, 1)',
    ACCENT = 'rgb(36, 199, 63)'
}
