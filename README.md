# Livrex

Livrex est un prototype d'un projet web permettant de répartir la livraison de paquets entre plusieurs livreurs. Ceci est la partie frontend implémentée à l'aide d'Angular.
## Usage  
Le site est accessible à l'adresse suivante : https://livrex.web.app
  
  1. Après avoir cliqué sur "Start a new course", une page vous permettant d'entrer les items accompagnés de leur position (x,y) vous sera présentée. Les coches définissent les éléments que vous souhaiter valider et une nouvelle ligne s'ajoute lorsque vous remplissez la dernière.
  2. Après cela, vous aurez la possibilité d'entrer les conducteurs accompagnés de leur position comme avant.
  3. Une fois que tout est en ordre, vous pouvez soumettre les informations en cliquant sur "submit", ce qui vous amènera sur une page de chargement le temps que les informations soient traitées. (Il faut prendre garde à ne pas mettre la localisation d'un conducteur à plus de 240 km de la base)
  4. Si les informations soumises sont adéquates, un graphe représentant les trajets calculés par le backend vous sera présenté. Sur cette page vous pouvez :
     1. Changer les couleurs des chemins
     2. Ajuster le rayon des points, la largeur des lignes
     3. Zoomer ou dézoomer
     4. Déplacer le graphe, réinitialiser sa position
     5. Afficher certains chemins avec plus ou moins de détails

## Démarche
Je suis parti sur l'idée de faire le projet en Angular, car c'est un framework que je trouve est pratique et offre beaucoup de possibilités pour le rendu visuel. La partie qui m'a prise le plus de temps est bien évidemment la visualisation des données sur un graphe.<br>J'ai d'abord commencé par chercher des librairies javascript qui pourraient me faciliter le travail. Lors de ma recherche, je suis tombé sur plusieurs librairies comme CanvaJS, ainsi que Cytoscape.js, qui proposaient des solutions intéressantes. Cependant, ces dernières étaient assez restrictives et n'étaient pas toujours aisément personnalisables.<br>C'est pourquoi j'ai implémenté ma propre approche pour la visualisation des données qui m'a permis de directement implémenter mes idées. 